/*
 * Copyright © 2014, Rafal Jastrzebski mail@nevar.pl
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
 * IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "QIRCPlugin.h"
#include <QtQuick>
#include <QDebug>
#include "QIRC/Model/IRCChannelMessageModel.h"
#include "QIRC/Model/IRCModel.h"

void QIRCPlugin::registerTypes(const char *uri)
{
    Q_ASSERT(uri == QLatin1String("QIRCPlugin"));
    qmlRegisterType<QIRCConnection>(uri, 1, 0, "QIRCConnection");
    qmlRegisterUncreatableType<IRCChannelMessageModel>(uri, 1, 0, "QIRCChannelMessageModel", "reason");
    qmlRegisterUncreatableType<IRCModel>(uri, 1, 0, "IRCModel", "reason");
}
