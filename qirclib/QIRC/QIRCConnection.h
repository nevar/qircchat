/*
 * Copyright © 2014, Rafal Jastrzebski mail@nevar.pl
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
 * IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef QIRCCONNECTION_H
#define QIRCCONNECTION_H

#include <QObject>
#include <QtQuick>
#include "QIRC/Model/IRCChannelMessageModel.h"

class QIRCConnectionPrivate;
class QIRCConnection : public QObject
{
    Q_OBJECT

public:
    Q_PROPERTY(ConnectionState connectionState READ connectionState NOTIFY connectionStateChanged)

    Q_ENUMS(ConnectionState)

    enum ConnectionState
    {
        CONNECTION_STATE_DISCONNECTED = 0,
        CONNECTION_STATE_CONNECTEDING,
        CONNECTION_STATE_CONNECTED
    };

    Q_INVOKABLE void connectToHost(const QString& host, const qint16 port, const QString& nick, const QString& name, const QString password = "" );

    Q_INVOKABLE void disconnectFromHost();

    Q_INVOKABLE IRCChannelMessageModel *getChannelModel(const QVariant& channelName);

public:
    explicit QIRCConnection(QObject *parent = 0);

    QIRCConnection::ConnectionState connectionState() const;

signals:
    void connectionStateChanged(QIRCConnection::ConnectionState state);

    void userAdded(const QString userName);

    void channelAdded(const QString channelName);

    void error(const QString errorMessage);

    void log(const QString message);

public slots:

private:
    QIRCConnectionPrivate* d_ptr;

};

#endif // QIRCCONNECTION_H
