/*
 * Copyright © 2014, Rafal Jastrzebski mail@nevar.pl
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
 * IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef IRCCHANNEL_H
#define IRCCHANNEL_H

#include "QIRC/QIRCConnection_p.h"
#include "QIRC/IRCUser.h"
#include "QIRC/Model/IRCChannelMessageModel.h"
#include <QObject>

class IRCChannel
{
public:
    IRCChannel( QIRCConnectionPrivate* pConnection, const QString channelName );
    ~IRCChannel();

    QString name() const;

    void insertMessage( const IRCUser* pUser, const QIRCConnectionPrivate::MessageType type, const QString& message);

    static QString toRawName(const QString& name);

    IRCChannelMessageModel* model();

    void populate();

    void addUser(IRCUser* pUser);

    void removeUser(IRCUser *pUser);

private:
    QIRCConnectionPrivate* m_pConnection;

    QString m_name;

    IRCChannelMessageModel* m_pModel;

    QHash<QString,IRCUser*> m_users;
};

#endif // IRCCHANNEL_H
