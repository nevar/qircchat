/*
 * Copyright © 2014, Rafal Jastrzebski mail@nevar.pl
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
 * IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "ServerInfo.h"
#include "QIRC/Log.h"
#include <QStringList>

ServerInfo::ServerInfo()
{
}

bool ServerInfo::isSet(const QString &option) const
{
    return m_options.contains( option );
}

QString ServerInfo::value(const QString &key) const
{
    return m_options.value(key);
}

void ServerInfo::clear()
{
    m_options.clear();
}

void ServerInfo::parseOptions(const QString &options)
{
    QStringList list = options.split(' ');
    foreach (QString optionString, list)
    {
        QStringList option = optionString.split('=');
        QString key = option.at(0);
        QString value;
        if ( option.count() > 1 )
            value = option.at(1);

        m_options.insert( key, value );
    }
}

QString ServerInfo::toString() const
{
    QString parsed;
    QHash<QString,QString>::ConstIterator ii = m_options.constBegin();

    while ( ii != m_options.end() )
    {
        if ( ! parsed.isEmpty() )
            parsed.append(' ');

        if ( ii.value().isEmpty() )
        {
            parsed.append( ii.key() );
        }
        else
        {
            parsed.append( ii.key() + "=" + ii.value() );
        }

        parsed.append( "\r\n" );
        ii++;
    }

    return parsed;
}
