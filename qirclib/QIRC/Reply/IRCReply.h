/*
 * Copyright © 2014, Rafal Jastrzebski mail@nevar.pl
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
 * IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef IRCREPLY_H
#define IRCREPLY_H

#include <QObject>
#include <QHash>
#include "QIRC/QIRCConnection_p.h"

class IRCReply;
typedef IRCReply* (*createFunction)(const IRCReply& request);

class IRCReply
{
private:
    void parse();

public:
    IRCReply(QIRCConnectionPrivate* pConnecton, const QString& line = QString());

    IRCReply(const IRCReply& request);

    virtual void exec();

    QString prefix() const;

    QString message() const;

    QString command() const;

    QString baseRequest() const;

    static IRCReply* parseRequest(QIRCConnectionPrivate* pConnecton, const QString& line);

protected:
    QIRCConnectionPrivate* m_pConnection;

private:
    const QString m_baseRequest;
    QString m_prefix;
    QString m_command;

    static QHash<QString,createFunction>* m_pKnownRequests;

protected:
    QList<QString> m_params;

public:
    static void fillKnownRequests();
    static void cleanKnownRequests();
};

#endif // IRCREPLY_H
