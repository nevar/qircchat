/*
 * Copyright © 2014, Rafal Jastrzebski mail@nevar.pl
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
 * IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "IRCReplyPing.h"
#include "QIRC/Request/IRCRequestPong.h"

IRCReplyPing::IRCReplyPing(const IRCReply &reply)
    : IRCReply( reply )
{
}

void IRCReplyPing::exec()
{
    if ( m_params.count() > 0 )
    {
        IRCRequest* pRequest = new IRCRequestPong( m_params.last() );
        m_pConnection->sendRequest( pRequest );
    }
}

IRCReply* createRequestPing(const IRCReply &reply)
{
    return new IRCReplyPing( reply );
}

