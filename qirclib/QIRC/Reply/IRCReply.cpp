/*
 * Copyright © 2014, Rafal Jastrzebski mail@nevar.pl
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
 * IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "IRCReply.h"
#include "QIRC/Log.h"
#include "QIRC/Reply/IRCReplyNotice.h"
#include "QIRC/Reply/IRCReplySystem.h"
#include "QIRC/Reply/IRCReplyMode.h"
#include "QIRC/Reply/IRCReply005.h"
#include "QIRC/Reply/IRCReplyMOTD.h"

QHash<QString,createFunction>* IRCReply::m_pKnownRequests = NULL;


IRCReply::IRCReply(const IRCReply &request)
    : m_pConnection( request.m_pConnection )
    , m_baseRequest( request.m_baseRequest )
{
    parse();
}

IRCReply::IRCReply(QIRCConnectionPrivate *pConnecton, const QString &line)
    : m_pConnection( pConnecton )
    , m_baseRequest( line )
{
    parse();
}

void IRCReply::parse()
{
    if ( m_baseRequest.isEmpty() )
    {
        return;
    }

    QString l( m_baseRequest );
    int index = 0;

    // prefix
    if ( l[0] == ':' ) {
        l.remove( 0, 1 );

        index = l.indexOf(" ");
        if ( index > 0 )
        {
            m_prefix = l.left( index );
            l.remove( 0, index + 1 );
        }
        else
        {
            m_prefix = l;
            l.clear();
            return;
        }
    }

    // command
    index = l.indexOf(" ");
    if ( index > 0 )
    {
        m_command = l.left( index );
        l.remove( 0, index + 1 );
    }
    else
    {
        m_command = l;
        l.clear();
        return;
    }

    // params
    while( m_params.count() < 14 )
    {
        if( l[0] == ':' )
        {
            l.remove( 0, 1 );
            m_params.append( l );
            return;
        }

        index = l.indexOf(" ");
        if ( index > 0 )
        {
            m_params.append( l.left( index ) );
            l.remove( 0, index + 1 );
        }
        else
        {
            m_params.append( l );
            return;
        }
    }
}

void IRCReply::exec()
{
}

QString IRCReply::prefix() const
{
    return m_prefix;
}

QString IRCReply::message() const
{
    return QString(m_params.last());
}

QString IRCReply::command() const
{
    return m_command;
}

QString IRCReply::baseRequest() const
{
    return m_baseRequest;
}

IRCReply *IRCReply::parseRequest(QIRCConnectionPrivate *pConnection, const QString &line)
{
    IRCReply base( pConnection, line );
    IRCReply* pRequest = NULL;

    if ( m_pKnownRequests->contains( base.command() ) )
    {
        pRequest = (*(*m_pKnownRequests)[base.command()])( base );
    }
    else
    {
        pRequest = new IRCReply(base);
    }

    return pRequest;
}

void IRCReply::fillKnownRequests()
{
    if ( ! m_pKnownRequests )
    {
        m_pKnownRequests = new QHash<QString,createFunction>();
        m_pKnownRequests->insert("NOTICE", createRequestNotice);
        m_pKnownRequests->insert("MODE", createRequestMode);
        m_pKnownRequests->insert("005", createRequest005);
        m_pKnownRequests->insert("372", createRequestMOTDAppend);
        m_pKnownRequests->insert("375", createRequestMOTDStart);
        m_pKnownRequests->insert("376", createRequestMOTDAppend);
        // All unhandled number commands will be handled by IRCReplySystem

        for ( int i = 1; i < 999; i++ )
        {
            QString str = QString("%1").arg(i, 3, 10, QChar('0') );
            if ( ! m_pKnownRequests->contains(str) )
                m_pKnownRequests->insert(str, createRequestSystem);
        }
    }
}

void IRCReply::cleanKnownRequests()
{
    m_pKnownRequests->clear();
    delete m_pKnownRequests;
}
