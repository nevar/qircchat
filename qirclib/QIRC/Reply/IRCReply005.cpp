/*
 * Copyright © 2014, Rafal Jastrzebski mail@nevar.pl
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
 * IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "IRCReply005.h"

IRCReply005::IRCReply005(const IRCReply &request)
    : IRCReply( request )
{
}

void IRCReply005::exec()
{
    QString message;
    if ( m_params.count() > 1 )
    {
        QList<QString>::iterator ii = m_params.begin();
        ii++;
        while ( ii != m_params.end() )
        {
            if ( *ii != m_params.last() )
            {
                m_pConnection->m_info.parseOptions( *ii );
            }

            if ( ! message.isEmpty() )
                message.append(" ");

            message.append( *ii );
            ii++;
        }

        m_pConnection->messageToChannel( QIRCConnectionPrivate::MESSAGE_TYPE_NOTICE,
                                         m_pConnection->host(),
                                         m_pConnection->host(),
                                         message );
    }

}

IRCReply* createRequest005(const IRCReply &request)
{
    return new IRCReply005( request );
}
