/*
 * Copyright © 2014, Rafal Jastrzebski mail@nevar.pl
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
 * IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "IRCReplyMode.h"
#include "QIRC/Log.h"

IRCReplyMode::IRCReplyMode(const IRCReply &request)
    : IRCReply( request )
{
}

void IRCReplyMode::exec()
{
    if ( m_params.count() < 2 )
        return;

    if ( prefix() == m_params[0] )
    {
        m_pConnection->messageToChannel( QIRCConnectionPrivate::MESSAGE_TYPE_MODE,
                                        m_pConnection->host(),
                                        m_pConnection->host(),
                                        "Mode changed for user " + m_params[0] + ": " + m_params[1] );
        m_pConnection->changeMode( m_params[0], m_params[1], m_pConnection->host() );
    }
    else
    {
        m_pConnection->messageToChannel( QIRCConnectionPrivate::MESSAGE_TYPE_MODE,
                                        prefix(),
                                        m_params[0],
                                        prefix() + "sets mode: " + m_params[1] + " for " + m_params[0] );
        m_pConnection->changeMode( m_params[0], m_params[1], prefix() );
    }
}

IRCReply *createRequestMode(const IRCReply &request)
{
    return new IRCReplyMode( request );
}
