/*
 * Copyright © 2014, Rafal Jastrzebski mail@nevar.pl
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
 * IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "QIRCConnection_p.h"
#include "QIRC/Log.h"
#include "QIRC/Reply/IRCReply.h"
#include "QIRC/IRCChannel.h"
#include "QIRC/IRCUser.h"
#include "QIRC/Request/IRCRequest.h"
#include "QIRC/Request/IRCRequestPassword.h"
#include "QIRC/Request/IRCRequestNick.h"
#include "QIRC/Request/IRCRequestUser.h"
#include "QIRC/Request/IRCRequestJoin.h"

QIRCConnectionPrivate::QIRCConnectionPrivate(QIRCConnection *q)
    : QObject( (QObject*)q )
    , q_ptr( q )
    , m_socket( this )
{
    m_socket.setObjectName("socket");
    QMetaObject::connectSlotsByName(this);
    IRCReply::fillKnownRequests();
}

QIRCConnectionPrivate::~QIRCConnectionPrivate()
{
    disconnectFromHost();
    IRCReply::cleanKnownRequests();
}

void QIRCConnectionPrivate::connectToHost(const QString &host, const qint16 port, const QString &nick, const QString& name, const QString &password)
{
    disconnectFromHost();
    QString rawNick = createUser( nick, NULL );
    m_users.value(rawNick)->setPassword( password );
    m_users.value(rawNick)->setName(name);
    m_myNick = rawNick;

    m_socket.connectToHost(host, port);
}

void QIRCConnectionPrivate::disconnectFromHost()
{
    m_socket.disconnectFromHost();
    clearUsersAndChannels();
    m_info.clear();
}

QIRCConnection::ConnectionState QIRCConnectionPrivate::connectionState() const
{
    if ( m_socket.state() == QAbstractSocket::ConnectedState )
    {
        return QIRCConnection::CONNECTION_STATE_CONNECTED;
    }
    else if ( m_socket.state() == QAbstractSocket::UnconnectedState )
    {
        return QIRCConnection::CONNECTION_STATE_DISCONNECTED;
    }

    return QIRCConnection::CONNECTION_STATE_CONNECTEDING;
}

void QIRCConnectionPrivate::messageToChannel(const QIRCConnectionPrivate::MessageType type, const QString &channelName, const QString &nick, const QString &message)
{
    QString rawChannelName = createChannel(channelName);
    QString rawNick = createUser(nick, m_channels.value(rawChannelName) );

    m_channels.value(rawChannelName)->insertMessage( m_users.value(rawNick), type, message);
}

QString QIRCConnectionPrivate::host() const
{
    return m_socket.peerName();
}

void QIRCConnectionPrivate::changeMode(const QString &who, const QString &mode, const QString &channelName)
{
    QString rawChannelName = createChannel(channelName);
    QString rawNick = createUser( who, m_channels.value(rawChannelName) );

    QChar changeType = ' ';
    for ( int i = 0; i < mode.count(); i++ )
    {
        if ( mode[i] == '-' || mode[i] == '+' )
        {
            changeType = mode[i];
        }
        else
        {
            if ( changeType == '-' )
            {
                m_users.value(rawNick)->removeUserMode( mode[i] );
            }
            else
            {
                m_users.value(rawNick)->addUserMode( mode[i] );
            }
        }
    }
}

QDir QIRCConnectionPrivate::channelBaseDir(const QString &channelName) const
{
    QDir dir = QDir::currentPath();
    if ( ! dir.exists( host() ) )
        dir.mkdir( host() );

    dir.cd( host() );

    if ( ! dir.exists( channelName ) )
        dir.mkdir(channelName);

    dir.cd( channelName );

    return dir;
}

QDir QIRCConnectionPrivate::logRequestBaseDir() const
{
    QDir dir = QDir::currentPath();
    dir.cd( "log" );

    return dir;
}

QDir QIRCConnectionPrivate::unknownRequestBaseDir() const
{
    QDir dir = QDir::currentPath();
    dir.cd( "unknown" );

    return dir;
}

void QIRCConnectionPrivate::sendRequest(IRCRequest *pRequest)
{
    QString toSend(pRequest->toString());
    OUT(toSend);
    saveToLog( false, toSend );
    toSend.append("\r\n");
    m_socket.write( toSend.toLatin1() );
    delete pRequest;
}

void QIRCConnectionPrivate::clearUsersAndChannels()
{
    QHash<QString,IRCChannel*>::iterator ii = m_channels.begin();
    while ( ii != m_channels.end() )
    {
        delete ii.value();
        ii++;
    }
    m_channels.clear();

    QHash<QString,IRCUser*>::iterator jj = m_users.begin();
    while ( jj != m_users.end() )
    {
        delete jj.value();
        jj++;
    }
    m_users.clear();
}

void QIRCConnectionPrivate::processLine(const QString &line)
{
    IN(line);
    saveToLog( true, line );
    IRCReply* pRequest = IRCReply::parseRequest( this, line );
    processRequest( pRequest );
}

void QIRCConnectionPrivate::processRequest(IRCReply *pRequest)
{
    pRequest->exec();
    delete pRequest;
}

QString QIRCConnectionPrivate::createUser(const QString &nick, IRCChannel* pChannel)
{
    QString rawNick = IRCUser::toRawNick(nick);
    if ( ! m_users.contains( rawNick ) )
    {
        IRCUser* pUser = new IRCUser(this, rawNick);
        if ( pChannel )
            pChannel->addUser( pUser );

        m_users.insert( rawNick, pUser );
        q_ptr->userAdded( rawNick );
    }

    return rawNick;
}

QString QIRCConnectionPrivate::createChannel(const QString &name)
{
    if ( name == "nevar5" )
        qDebug() << "";

    QString rawChannelName = IRCChannel::toRawName(name);
    if ( ! m_channels.contains( rawChannelName ) )
    {
        IRCChannel* pChan = new IRCChannel(this, rawChannelName);
        pChan->populate();
        m_channels.insert( rawChannelName, pChan );
        q_ptr->channelAdded( rawChannelName );
    }

    return rawChannelName;
}

void QIRCConnectionPrivate::saveToLog(bool isReply, const QString &data)
{
    if ( m_settings.saveAllRequests() )
    {
        QDir dir = channelBaseDir("log");
        QFile file( dir.absoluteFilePath("all") );
        file.open( QFile::ReadWrite | QFile::Append );
        file.seek( file.size() );
        QString dataToWrite;
        if ( isReply )
        {
            dataToWrite = "[ IN]";
        }
        else
        {
            dataToWrite = "[OUT]";
        }
        dataToWrite += data + "\r\n";
        file.write( dataToWrite.toLatin1() );
        file.close();
    }
}

void QIRCConnectionPrivate::on_socket_stateChanged(QAbstractSocket::SocketState socketState)
{
    static QIRCConnection::ConnectionState lastState = QIRCConnection::CONNECTION_STATE_DISCONNECTED;

    if ( lastState != connectionState() )
    {
        lastState = connectionState();
        if ( lastState == QIRCConnection::CONNECTION_STATE_CONNECTED )
        {
            if ( m_users.contains(m_myNick) && ! m_users.value(m_myNick)->password().isEmpty() )
            {
                IRCRequestPassword* pRequest = new IRCRequestPassword( m_users.value(m_myNick)->password() );
                sendRequest( pRequest );
            }

            sendRequest( new IRCRequestNick( m_myNick ) );
            sendRequest( new IRCRequestUser( m_myNick, 0, m_users.value(m_myNick)->name() ) );
            sendRequest( new IRCRequestJoin(this, "#help") );
        }
        else if ( lastState == QIRCConnection::CONNECTION_STATE_DISCONNECTED )
        {
        }

        emit q_ptr->connectionStateChanged( lastState );
    }
}

void QIRCConnectionPrivate::on_socket_error(QAbstractSocket::SocketError socketError)
{
    ERROR( "error: " << m_socket.errorString() );
    emit q_ptr->error( m_socket.errorString() );
}

void QIRCConnectionPrivate::on_socket_readyRead()
{
    static QByteArray data;
    QByteArray currentData = m_socket.readAll();
    data.append(currentData);
    data.replace('\r', QByteArray());

    QList<QByteArray> lines = data.split('\n');

    for ( int i = 0; i < lines.count(); i++ )
    {
        if ( i == lines.count() - 1 )
        {
            if ( ! lines[i].isEmpty() )
            {
                data = lines[i];
            }
            else
            {
                data = "";
            }
            return;
        }

        if ( !lines[i].isEmpty() )
        {
            processLine( QString( lines[i] ) );
        }
    }
}
