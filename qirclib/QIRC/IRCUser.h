/*
 * Copyright © 2014, Rafal Jastrzebski mail@nevar.pl
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
 * IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef IRCUSER_H
#define IRCUSER_H

#include "QIRC/QIRCConnection_p.h"
#include <QObject>

class IRCUser
{
public:
    IRCUser( QIRCConnectionPrivate* pConnection, const QString& nick);

    QString nick() const;

    QString rawNick() const;

    static void cleanUserMods();

    static QChar userModeToChar();

    static QChar charToUserMode();

    void addUserMode(QChar mode);

    void removeUserMode(QChar mode);

    static void defineUserMode(QChar mode, QChar qchar);

    static QString toRawNick( const QString& nick);

    void addChannel(IRCChannel* pChannel);

    void removeChannel(IRCChannel* pChannel);

    void setPassword(const QString& password);
    QString password() const;

    void setName(const QString& name);
    QString name() const;

private:
    QString m_name;

    QString m_nick;

    QString m_password;

    QHash<QString,IRCChannel*> m_channels;

    QList<QChar> m_userMods;

    static QHash<QChar, QChar> m_supportedUserMods;
};

#endif // IRCUSER_H
