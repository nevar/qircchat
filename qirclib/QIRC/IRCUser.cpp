/*
 * Copyright © 2014, Rafal Jastrzebski mail@nevar.pl
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
 * IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "IRCUser.h"
#include "QIRC/Log.h"

QHash<QChar, QChar> IRCUser::m_supportedUserMods;

IRCUser::IRCUser(QIRCConnectionPrivate* pConnection, const QString &nick )
    : m_nick( nick )
{
}

QString IRCUser::nick() const
{
    return m_nick;
}

QString IRCUser::rawNick() const
{
    return m_nick;
}

void IRCUser::cleanUserMods()
{
    m_supportedUserMods.clear();
}

void IRCUser::defineUserMode(QChar mode, QChar qchar)
{
    // TODO implement
}

QString IRCUser::toRawNick(const QString &nick)
{
    QString rawNick = nick;

    QHash<QChar,QChar>::iterator i = m_supportedUserMods.begin();
    for ( ; i != m_supportedUserMods.end(); i++ )
    {
        if ( rawNick.startsWith( i.value() ) )
        {
            rawNick.remove(0, 1);
        }
    }

    return rawNick;
}

void IRCUser::addChannel(IRCChannel *pChannel)
{
    // TODO implement
}

void IRCUser::removeChannel(IRCChannel *pChannel)
{
    // TODO implement
}

void IRCUser::setPassword(const QString &password)
{
    m_password = password;
}

QString IRCUser::password() const
{
    return m_password;
}

void IRCUser::setName(const QString &name)
{
    m_name = name;
}

QString IRCUser::name() const
{
    return m_name;
}

QChar IRCUser::userModeToChar()
{
    // TODO implement
}

QChar IRCUser::charToUserMode()
{
    // TODO implement
}

void IRCUser::addUserMode(QChar mode)
{
    DEBUG( "adding mode " << mode);
    m_userMods.insert(0, mode);
}

void IRCUser::removeUserMode(QChar mode)
{
    DEBUG( "remove mode " << mode);
    m_userMods.removeAll( mode );
}
