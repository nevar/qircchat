/*
 * Copyright © 2014, Rafal Jastrzebski mail@nevar.pl
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
 * IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef LOG_H
#define LOG_H

#include <QDebug>

//#define OUT(x) qDebug() << "[REQ]" << __FUNCTION__ << ":" << __LINE__ << ">" << x
//#define IN(x) qDebug() << "[RPL]" << __FUNCTION__ << ":" << __LINE__ << ">" << x

#define DEBUG(x) qDebug() << "[D]" << __FUNCTION__ << ":" << __LINE__ << ">" << x
#define INFO(x) qDebug() << "[I]"<< __FUNCTION__ << ":" << __LINE__ << ">" << x
#define ERROR(x) qDebug() << "[E]"<< __FUNCTION__ << ":" << __LINE__ << ">" << x

#ifndef OUT
    #define OUT(x)
#endif

#ifndef IN
    #define IN(x)
#endif

#ifndef DEBUG
    #define DEBUG(x)
#endif

#ifndef INFO
    #define INFO(x)
#endif

#ifndef ERROR
    #define ERROR(x)
#endif

#endif // LOG_H
