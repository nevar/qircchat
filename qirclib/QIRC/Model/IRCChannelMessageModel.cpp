#include "IRCChannelMessageModel.h"
#include "QIRC/QIRCConnection_p.h"
#include "QIRC/Log.h"
#include <QDateTime>
#include <QEvent>

IRCChannelMessageModel::IRCChannelMessageModel(QIRCConnectionPrivate *pConnection, const QString &channelName)
    : QAbstractListModel(NULL)
    , m_pConnection( pConnection )
    , m_channelName( channelName )
{
    cleanData();
}

IRCChannelMessageModel::~IRCChannelMessageModel()
{
    cleanData();
}

void IRCChannelMessageModel::populate()
{
    cleanData();

    if ( m_pConnection->m_settings.saveChannelMessages() )
    {
        QDir baseDir = m_pConnection->channelBaseDir( m_channelName );
        QFileInfoList files = baseDir.entryInfoList( QDir::Files, QDir::Name );

        if ( ! files.isEmpty() )
        {
            QFileInfoList::iterator ii = files.end();
            if ( ii != files.begin() ) ii--;
            if ( ii != files.begin() ) ii--;

            while ( ii != files.end() )
            {
                loadFromFile( ii->fileName() );
                ii++;
            }
        }
    }
}

void IRCChannelMessageModel::insertMessage(const QString &line)
{
    QString aCurrentDate = currentDate();
    if ( m_logDays.count() == 0 || m_logDays.last().logName != aCurrentDate )
    {
        // create new log day
        LogDayData newLogDayData;
        newLogDayData.logName = aCurrentDate;
        m_logDays.append( newLogDayData );

        // insert initial message
        appendMessage("===== " + aCurrentDate + " =====");
    }

    appendMessage(line);
    if ( m_pConnection->m_settings.saveChannelMessages() )
    {
        QDir channelDir = m_pConnection->channelBaseDir( m_channelName );
        QFile file( channelDir.absoluteFilePath( currentDate() ) );
        file.open( QFile::Append );
        file.seek( file.size() );
        file.write( line.toLatin1() );
        file.write( "\r\n" );
        file.close();
    }
}

void IRCChannelMessageModel::loadFromFile(const QString &fileName)
{
    QDir channelDir = m_pConnection->channelBaseDir( m_channelName );
    QFile file( channelDir.absoluteFilePath( fileName ) );

    foreach (LogDayData fileData, m_logDays) {
        if ( fileData.logName == fileName )
        {
            ERROR("file already loaded: " << fileName << channelDir );
            return;
        }
    }

    m_logDays.append( LogDayData() );
    LogDayData& fileData = m_logDays.last();
    fileData.logName = fileName;

    if ( ! file.isOpen() )
        file.open( QFile::ReadOnly );

    QTextStream inStream( &file );
    INFO( "loading messages from: " << fileName );

    beginInsertRows( QModelIndex(), m_messagesCount, m_messagesCount );
    fileData.messages.insert( fileData.messages.count(), "===== " + fileName + " =====" );
    m_messagesCount++;
    endInsertRows();

    while ( ! inStream.atEnd() )
    {
        QString line = inStream.readLine();
        beginInsertRows( QModelIndex(), m_messagesCount, m_messagesCount );
        fileData.messages.insert( fileData.messages.count(), line );
        m_messagesCount++;
        endInsertRows();
    }

    file.close();
}

void IRCChannelMessageModel::cleanData()
{
    beginRemoveRows( QModelIndex(), 0, m_messagesCount );
    foreach (LogDayData fileData, m_logDays)
    {
        fileData.messages.clear();
    }

    m_logDays.clear();
    m_messagesCount = 0;
    endRemoveRows();
}

void IRCChannelMessageModel::appendMessage(const QString &message)
{
    beginInsertRows( QModelIndex(), m_messagesCount, m_messagesCount );
    m_logDays.last().messages.insert( m_logDays.last().messages.count(), message );
    m_messagesCount++;
    endInsertRows();
}

QVariant IRCChannelMessageModel::data(const QModelIndex &index, int role) const
{
    if ( index.isValid() && role == ROLE_DATA && m_messagesCount > index.row() )
    {
        QList<LogDayData>::const_iterator ii = m_logDays.begin();
        int count = 0;
        while ( ii != m_logDays.end() )
        {
            if ( count + (*ii).messages.count() > index.row() )
            {
                QVariant v = (*ii).messages.value( index.row() - count );
                return v;
            }
            count += (*ii).messages.count();
            ii++;
        }
    }

    return QVariant();
}

int IRCChannelMessageModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return m_messagesCount;
}

QHash<int, QByteArray> IRCChannelMessageModel::roleNames() const
{
    QHash<int, QByteArray> hash;

    hash.insert( ROLE_DATA, "messageData" );

    return hash;
}

QString IRCChannelMessageModel::currentDate()
{
    return QDateTime::currentDateTime().toString("yyyy-MM-dd");
}

QString IRCChannelMessageModel::currentTime()
{
    return QDateTime::currentDateTime().toString("[HH:mm:ss]");
}
