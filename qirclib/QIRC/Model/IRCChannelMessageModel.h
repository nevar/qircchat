#ifndef IRCCHANNELMESSAGEMODEL_H
#define IRCCHANNELMESSAGEMODEL_H

#include <QObject>
#include <QAbstractListModel>
#include <QStringList>
#include <QDir>

class QIRCConnectionPrivate;
class IRCChannelMessageModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum Roles
    {
        ROLE_DATA = 0
    };

    struct LogDayData
    {
        QString logName;
        QMap<int,QString> messages;
    };

    explicit IRCChannelMessageModel(QIRCConnectionPrivate* pConnection, const QString& channelName);

    ~IRCChannelMessageModel();

    void populate();

    void insertMessage(const QString& line);

private:
    void loadFromFile(const QString &fileName);

    void cleanData();

    void appendMessage(const QString& message);

public:
    static QString currentDate();
    
    static QString currentTime();

private:
    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;

    virtual QHash<int, QByteArray> roleNames() const;

private:
    QIRCConnectionPrivate* m_pConnection;
    QList<LogDayData> m_logDays;
    int m_messagesCount;
    QString m_channelName;

signals:
    
public slots:
    
};

#endif // IRCCHANNELMESSAGEMODEL_H
