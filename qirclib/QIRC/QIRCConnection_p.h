/*
 * Copyright © 2014, Rafal Jastrzebski mail@nevar.pl
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
 * OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
 * IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef QIRCCONNECTION_P_H
#define QIRCCONNECTION_P_H

#include <QObject>
#include <QTcpSocket>
#include <QHash>
#include "QIRC/QIRCConnection.h"
#include "QIRC/IRCSettings.h"
#include "QIRC/ServerInfo.h"

class IRCReply;
class IRCRequest;
class IRCChannel;
class IRCUser;
class IRCData;

class QIRCConnectionPrivate : public QObject
{
    friend class QIRCConnection;
    Q_OBJECT

public:
    // Type of message passed to channel
    enum MessageType
    {
        MESSAGE_TYPE_NOTICE = 0,
        MESSAGE_TYPE_MODE
    };

public:
    explicit QIRCConnectionPrivate(QIRCConnection *q);
    ~QIRCConnectionPrivate();

    void connectToHost(const QString& host, const qint16 port, const QString& nick, const QString &name, const QString& password);

    void disconnectFromHost();

    QIRCConnection::ConnectionState connectionState() const;

    void messageToChannel(const MessageType type, const QString& channelName, const QString& nick, const QString &message);

    QString host() const;

    void changeMode(const QString& who, const QString& mode, const QString& channelName );

    QDir channelBaseDir( const QString& channelName ) const;

    QDir logRequestBaseDir() const;

    QDir unknownRequestBaseDir() const;

    void sendRequest( IRCRequest* request );

private:
    void clearUsersAndChannels();

    void processLine(const QString& line);

    void processRequest(IRCReply* pRequest);

    QString createUser(const QString& name, IRCChannel* pChannel);

    QString createChannel(const QString& name);

    void saveToLog( bool isReply, const QString& data);

signals:

public slots:
    void on_socket_stateChanged(QAbstractSocket::SocketState socketState);
    void on_socket_error(QAbstractSocket::SocketError socketError);
    void on_socket_readyRead();

private:
    QIRCConnection* q_ptr;

private:
    QHash<QString,IRCChannel*> m_channels;
    QHash<QString,IRCUser*> m_users;
    QTcpSocket m_socket;
    QString m_myNick;

public:
    ServerInfo m_info;
    IRCSettings m_settings;
    QString m_motd;
};

#endif // QIRCCONNECTION_P_H
