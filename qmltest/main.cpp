#include <QtGui/QGuiApplication>
#include <QQmlEngine>
#include <QDir>
#include "qtquick2applicationviewer.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QtQuick2ApplicationViewer viewer;
    QDir dir(QDir::currentPath());
    dir.cdUp();
    viewer.engine()->addImportPath( dir.absolutePath() );
    viewer.setMainQmlFile(QStringLiteral("qml/main.qml"));
    viewer.showExpanded();

    return app.exec();
}
