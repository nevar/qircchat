import QtQuick 2.0
import QtQuick.Controls 1.0
import QIRCPlugin 1.0

Rectangle {
    id: main
    width: 360
    height: 360

    Component.onCompleted:
    {
        state = "disconnected"
    }

    states: [
        State {
            name: "connected"
            PropertyChanges { target: connectButton; color: "lightgreen" }
            PropertyChanges { target: connectText; text: "connected" }
        },
        State {
            name: "connecting"
            PropertyChanges { target: connectButton; color: "lightblue" }
            PropertyChanges { target: connectText; text: "connecting ..." }
        },
        State {
            name: "disconnected"
            PropertyChanges { target: connectButton; color: "red" }
            PropertyChanges { target: connectText; text: "disconnected" }
        }
    ]

    Rectangle
    {
        id: connectButton
        x: 0; y: 0
        width: 100; height: 30

        Text {
            id: connectText
            anchors.fill: parent
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if ( main.state === "disconnected" )
                {
                    ircConnection.connectToHost("kornbluth.freenode.net", 6667, "nevar5", "nvr")
//                    ircConnection.connectToHost("katowice.ircnet.pl", 6667)
                }
                else
                {
                    ircConnection.disconnectFromHost()
                }
            }
        }
    }

    TabView
    {
        id: tabControl
        anchors.top: connectButton.bottom
        anchors.bottom: main.bottom
        anchors.left: main.left
        anchors.right: main.right
    }

    QIRCConnection
    {
        id: ircConnection

        onConnectionStateChanged:
        {
            if ( ircConnection.connectionState === QIRCConnection.CONNECTION_STATE_CONNECTED )
            {
                main.state = "connected"
            }
            else if( ircConnection.connectionState === QIRCConnection.CONNECTION_STATE_CONNECTEDING )
            {
                main.state = "connecting"
            }
            else
            {
                main.state = "disconnected"
            }
        }

        onUserAdded:
        {
            console.log("user added " + userName)
        }

        onChannelAdded:
        {
            tabControl.addTab(channelName, Qt.createComponent("Log2.qml"))
            tabControl.getTab( tabControl.count - 1 ).item.objectName = channelName
        }
    }
}
