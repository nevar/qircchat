import QtQuick 2.0
import QtQuick.Controls 1.0

Rectangle {
    id: main
    anchors.fill: parent
    color: "#EEEEEE"

    Connections
    {
        target: ircConnection
    }

    Component
    {
        id: del

        Text
        {
            height: 20
            width: textArea.width
            text: messageData
        }
    }

    ListView
    {
        id: textArea
        anchors.fill: parent
        delegate: del

        onCountChanged:
        {
            textArea.currentIndex = textArea.count - 1
        }
    }

    onObjectNameChanged:
    {
        textArea.model = ircConnection.getChannelModel( main.objectName )
    }
}
