import QtQuick 2.0
import QtQuick.Controls 1.0

Rectangle {
    anchors.fill: parent

    Connections
    {
        target: ircConnection

        onLog:
        {
            textArea.append(message)
        }
    }

    TextArea
    {
        id: textArea
        anchors.fill: parent
        readOnly: true
    }
}
